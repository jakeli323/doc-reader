package docreader.com.docreader.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import docreader.com.docreader.DocItem;
import docreader.com.docreader.JsonParser;
import docreader.com.docreader.R;
import docreader.com.docreader.activities.MainActivity;
import docreader.com.docreader.database.DBHelper;

/**
 * Created by Administrator on 2017-11-09.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder>{
    private Context mContext;
    private List<String> mDocNameList;
    private int selectedIndex = -1;
    private LinearLayoutManager mllm;
    private Toast mToast;
    private int curInstructIndex = 0;
    private CountDownTimer cdt;
    private int countDown = 0;
    private final int kCountDown = 8;
    private List<String> curInstructList;
    DBHelper dbHelper;

    public ItemAdapter(Context context, LinearLayoutManager llm, List<String> docNameList, DBHelper dbHelper){
        super();
        mContext = context;
        mDocNameList = docNameList;
        mllm = llm;
        this.dbHelper = dbHelper;
        Log.d("Bind", "***************8  doc list size = " + mDocNameList.size());
        mToast = Toast.makeText(mContext,"",Toast.LENGTH_SHORT);

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        Log.d("Bind", "****************    create view holder ");
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doc, parent, false);
        mContext = parent.getContext();
        return new ItemViewHolder(itemView);
    }

//    public abstract void onBindViewHolder(VH holder, int position);
    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position){
        //等等，这个position好像是倒过来的。
        final String name = mDocNameList.get(position);

        holder.vName.setText(name);

        if(selectedIndex == position){
            holder.vName.setTextColor(Color.RED);
        }else{
            holder.vName.setTextColor((Color.BLACK));
        }


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

 //                ((TextView)((LinearLayout)view).getChildAt(0)).setTextColor(Color.RED);
//                FlowerCollector.onEvent(MainActivity.this, "tts_play");
//
//                setParam();
//                int code = mSpeechSyn.startSpeaking(di.content, mSynCallbackListener);
//                if (code != ErrorCode.SUCCESS) {
//                    showTip("语音合成失败111,错误码: " + code);
//                }
//            }
                if(cdt != null){
                    cdt.cancel();
                    countDown = 0;
                }
                getSpeechSyn().stopSpeaking();

                curInstructList = dbHelper.getInstructList(position);

                instructSpeaking(position, 0);

                for(int i = 0; i < 20; i++){

                    View foundView = mllm.findViewByPosition(i);
                    if(foundView != null){
                        TextView tv = (TextView)(foundView.findViewById(R.id.file_name_text));
                        tv.setTextColor(Color.BLACK);
                    }
                }
                selectedIndex = position;
                View curView = mllm.findViewByPosition(position);
                TextView tv = (TextView)curView.findViewById(R.id.file_name_text);
                tv.setTextColor(Color.RED);
            }
        });
    }
    private void instructSpeaking(int pos, int index){
//        DocItem di = mDocList.get(pos);
//        dbHelper.getInstructList(pos)

        startSpeaking(curInstructList.get(index));
    }

    private void startSpeaking(String str){
        int code = getSpeechSyn().startSpeaking(str, mSynCallbackListener);
        getMainActicity().SetIsPlaying(true);

        if (code != ErrorCode.SUCCESS) {
            showTip("语音合成失败111,错误码: " + code);
            cdt.cancel();
            curInstructIndex = 0;

        }
    }

    private SynthesizerListener mSynCallbackListener = new SynthesizerListener() {

        @Override
        public void onSpeakBegin() {
            showTip("开始播放");
        }

        @Override
        public void onSpeakPaused() {
            showTip("暂停播放");
        }

        @Override
        public void onSpeakResumed() {
            showTip("继续播放");
        }

        @Override
        public void onBufferProgress(int percent, int beginPos, int endPos,
                                     String info) {
//            // 合成进度
//            mPercentForBuffering = percent;
//            showTip(String.format(getString(R.string.tts_toast_format),
//                    mPercentForBuffering, mPercentForPlaying));
        }

        @Override
        public void onSpeakProgress(int percent, int beginPos, int endPos) {
            // 播放进度
//            mPercentForPlaying = percent;
//            showTip(String.format(getString(R.string.tts_toast_format),
//                    mPercentForBuffering, mPercentForPlaying));
        }

        @Override
        public void onCompleted(SpeechError error) {
            if (error == null) {
                showTip("播放一条指令完成");
                curInstructIndex ++;

                if(curInstructIndex > curInstructList.size() - 1){
                    showTip("播放完成");
                    View curView = mllm.findViewByPosition(selectedIndex);
                    TextView tv = (TextView)curView.findViewById(R.id.file_name_text);
                    tv.setTextColor(Color.BLACK);
                    selectedIndex = -1;
                    curInstructIndex = 0;
                    getMainActicity().SetIsPlaying(false);
                    return;
                }
                Log.d("Bind", "OOOOOOOOOOOOOOO start listener");
                getMainActicity().getSpeechRecogn().startListening(mRecognizerListener);

//                if(cdt != null){
//                    cdt.cancel();
//                }
//                getSpeechSyn().stopSpeaking();
                countDown = kCountDown;
                Log.d("Bind", "**************** tick tick tick " + countDown);
                cdt = new CountDownTimer((countDown--) * 1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        Log.d("Bind", "**************** tick tick tick " + countDown);
                        getMainActicity().SetStopBtnText(String.valueOf(countDown--));
                    }

                    @Override
                    public void onFinish() {
                        Log.d("Bind", "**************** finish finish");
                        playNext();
                    }
                };
                cdt.start();
            } else if (error != null) {
                showTip(error.getPlainDescription(true));
            }
        }

        @Override
        public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {
            // 以下代码用于获取与云端的会话id，当业务出错时将会话id提供给技术支持人员，可用于查询会话日志，定位出错原因
            // 若使用本地能力，会话id为null
            //	if (SpeechEvent.EVENT_SESSION_ID == eventType) {
            //		String sid = obj.getString(SpeechEvent.KEY_EVENT_SESSION_ID);
            //		Log.d(TAG, "session id =" + sid);
            //	}
        }
    };

    private RecognizerListener mRecognizerListener = new RecognizerListener() {

        @Override
        public void onBeginOfSpeech() {
            // 此回调表示：sdk内部录音机已经准备好了，用户可以开始语音输入
            showTip("开始说话");
        }

        @Override
        public void onError(SpeechError error) {
            // Tips：
            // 错误码：10118(您没有说话)，可能是录音机权限被禁，需要提示用户打开应用的录音权限。
            // 如果使用本地功能（语记）需要提示用户开启语记的录音权限。
            if(!getSpeechSyn().isSpeaking()){
                Log.d("Bind", "OPPPPPPPPPPPPPPPPPP  start listener ");
                getMainActicity().getSpeechRecogn().startListening(mRecognizerListener);

            }

            if(false && error.getErrorCode() == 14002) {
                showTip( error.getPlainDescription(true)+"\n请确认是否已开通翻译功能" );
            } else {
                showTip(error.getPlainDescription(true));
            }
        }

        @Override
        public void onEndOfSpeech() {
            // 此回调表示：检测到了语音的尾端点，已经进入识别过程，不再接受语音输入
            showTip("结束说话");
            if(!getSpeechSyn().isSpeaking()){
                Log.d("Bind", "djjjdjdjdjdjdj start listenr");
                getMainActicity().getSpeechRecogn().startListening(mRecognizerListener);

            }
        }

        @Override
        public void onResult(RecognizerResult results, boolean isLast) {
            Log.d("Bind", results.getResultString());
//
//            {"sn":1,"ls":false,"bg":0,"ed":0,"ws":[{"bg":0,"cw":[{"sc":0.00,"w":"喂"}]},{"bg":0,"cw":[{"sc":0.00,"w":"喂"}]},{"bg":0,"cw":[{"sc":0.00,"w":"喂"}]},{"bg":0,"cw":[{"sc":0.00,"w":"喂"}]}]}
//            11-10 19:17:32.149 21516-21516/docreader.com.docreader D/Bind: {"sn":2,"ls":true,"bg":0,"ed":0,"ws":[{"bg":0,"cw":[{"sc":0.00,"w":"。"}]}]}

            String text = JsonParser.parseIatResult(results.getResultString());
            if(text.contains("停") || text.contains("听")  || text.contains("零")  ){//零
                //todo markmark 停
                cdt.cancel();
                Log.d("Bind", "))))))))))))))))) 停了");
                getMainActicity().getSpeechRecogn().startListening(mRecognizerListener);

                showTip("停止播放");
            }else if(text.contains("继续")){
//                getMainActicity().speechSynResume();

                playNext();
                Log.d("Bind", "))))))))))))))))) 继续");
                showTip("继续播放");
            }else {
                int num = getMainActicity().tryGetNum(text);
                if(num > 0){
                    cdt.cancel();
                    curInstructIndex = num;
                    playNext();
                    Log.d("Bind", "))))))))))))))))) 继续");
                    showTip("继续播放");
                }


            }
            Log.d("Bind", "))))))))))))))))) text  = " + text);

//            String sn = null;
//            try {
//                JSONObject resultJson = new JSONObject(results.getResultString());
//                sn = resultJson.optString("sn");
//                Log.d("Bind", "))))))))))))))))) sn  = " + sn);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }


            if (isLast) {
                // TODO 最后的结果
            }
        }

        @Override
        public void onVolumeChanged(int volume, byte[] data) {
            showTip("当前正在说话，音量大小：" + volume);
        }

        @Override
        public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {
            // 以下代码用于获取与云端的会话id，当业务出错时将会话id提供给技术支持人员，可用于查询会话日志，定位出错原因
            // 若使用本地能力，会话id为null
            //	if (SpeechEvent.EVENT_SESSION_ID == eventType) {
            //		String sid = obj.getString(SpeechEvent.KEY_EVENT_SESSION_ID);
            //		Log.d(TAG, "session id =" + sid);
            //	}
        }
    };

    @Override
    public int getItemCount(){
//        Log.d("Bind", "********************* get item count = " + mDocList.size());
        return mDocNameList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        protected TextView vName;
        protected View cardView;

        public ItemViewHolder(View v){
            super(v);
            vName = (TextView)v.findViewById(R.id.file_name_text);
            cardView = v.findViewById(R.id.card_view);
        }
    }

    private SpeechSynthesizer getSpeechSyn(){
        return getMainActicity().getSpeechSyn();
    }

    private MainActivity getMainActicity(){
       return (MainActivity)mContext;
    }

    private void showTip(final String str) {
        ((MainActivity)mContext).showTip(str);
    }

    private void playNext(){
        getMainActicity().SetStopBtnText("确定");
        getMainActicity().getSpeechRecogn().stopListening();

        instructSpeaking(selectedIndex, curInstructIndex);
        countDown = 0;
    }

    public void clearList(){
        mDocNameList.clear();
    }

    public void setList(List<String> list){
        mDocNameList = list;
    }

    public LinearLayoutManager getLayoutMgr(){
        return mllm;
    }
}
