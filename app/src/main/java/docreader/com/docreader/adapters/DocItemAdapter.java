package docreader.com.docreader.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import docreader.com.docreader.DocItem;
import docreader.com.docreader.R;
import docreader.com.docreader.Util;

/**
 * Created by Administrator on 2017-10-10.
 */

public class DocItemAdapter extends ArrayAdapter<DocItem> {
    public DocItemAdapter(Context context, int resource, List<DocItem> list){
        super(context, resource, list);
    }

    //TODO get
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id){
    //之所以会出现点击一个 两个标红，是因为click的时候，click到了两个东西，
    //或者两个东西都指向了一个引用

    //首先他是给mListView注册listener，listview下面的就是一堆adapterview，估计有两个
    //adapterview的getview，get出来是同一个 linearlayout
    //
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Util.print("gggggggggggggggget view positoin = " + position);
        DocItem item = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_doc, parent, false);
        }

        TextView docName = (TextView)convertView.findViewById(R.id.file_name_text);

        docName.setText(item.name);

        return convertView;
    }

    public void allBlack(){

    }


}
