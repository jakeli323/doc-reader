package docreader.com.docreader.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import docreader.com.docreader.activities.MainActivity;

/**
 * Created by Administrator on 2017-12-13.
 */

public class DBHelper extends SQLiteOpenHelper {
    private Context context;
    public String databaseName;
    public static final String TABLE_DOCNAMES = "doc_names";
    public static final String TABLE_INSTRUCTS = "instructs";
    public static final String FIELD_DOC_ID = "doc_id";
    public static final String FIELD_DOC_NAME = "doc_name";
    public static final String FIELD_INSTRUCT = "instruct";

    public DBHelper(Context context, String dbPath){
        super(context, dbPath, null, 1);
        this.context = context;

        Log.d("Bind",  "sososwererososo = "  + context.getPackageName());
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        Log.d("Bind", "is oncreate called every time new database helper??????????asdfasdf??????????????");
//        db.execSQL(
//                "create table contacts " +
//                        "(id integer primary key, name text,phone text,email text, street text,place text)"
//        );
        db.execSQL("CREATE TABLE " + TABLE_DOCNAMES + "(" +
                FIELD_DOC_ID + " integer PRIMARY KEY AUTOINCREMENT," +
                FIELD_DOC_NAME + " text NOT NULL" +
                ")");

        db.execSQL("CREATE TABLE " + TABLE_INSTRUCTS + "(" +
                "instruct_id integer PRIMARY KEY," +
                FIELD_DOC_ID + " integer ," +
                "instruct text" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }

    public void getData(){
        SQLiteDatabase db = getReadableDatabase();
    }

    public boolean insertDocname(int i, String docName){
        //todo 参照下面example 塞进去数据z
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FIELD_DOC_ID, i);
        contentValues.put(FIELD_DOC_NAME, docName);
        db.insert(TABLE_DOCNAMES, null, contentValues);
        return true;
    }

    public boolean insertInstruct(int i, String instruct){
        //todo 参照下面example 塞进去数据z
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FIELD_DOC_ID, i);
        contentValues.put(FIELD_INSTRUCT, instruct);
        db.insert(TABLE_INSTRUCTS, null, contentValues);
        return true;
    }

    public List<String> getDocNameList(){
        List<String> list = new ArrayList<String>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from " + TABLE_DOCNAMES,null);
        if(cursor != null){
            if(cursor.moveToFirst()){
                do{
                    String index = cursor.getString(cursor.getColumnIndex(FIELD_DOC_ID));
                    String docName = cursor.getString(cursor.getColumnIndex(FIELD_DOC_NAME));
                    Log.d("Bind", "EEOEEOEOEOEOE =  idnex = " + index);
                    Log.d("Bind", "EEOEEOEOEOEOE =  docname = " + docName);
                    list.add(docName);
                }while(cursor.moveToNext());
            }
        }
        cursor.close();
        Log.d("Bind", "fjfjfjfjfjff list = " + list.toString());
        return list;
    }

    public List<String> getInstructList(int docIndex){
        List<String> list = new ArrayList<String>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from " + TABLE_INSTRUCTS + "  where " + FIELD_DOC_ID + " = " + docIndex, null);
        if(cursor != null){
            if(cursor.moveToFirst()){
                do{
                    String index = cursor.getString(cursor.getColumnIndex(FIELD_DOC_ID));
                    String instruct = cursor.getString(cursor.getColumnIndex(FIELD_INSTRUCT));
                    Log.d("Bind", "EEOEEOEOEOEOE =  idnex = " + index);
                    Log.d("Bind", "EEOEEOEOEOEOE =  instruct = " + instruct);
                    list.add(instruct);
                }while(cursor.moveToNext());
            }
        }
        cursor.close();
        Log.d("Bind", "222222222 list = " + list.toString());
        return list;

    }

    public void dropAllTables(){
        getReadableDatabase().execSQL("DROP TABLE IF EXISTS " + TABLE_DOCNAMES);
        getReadableDatabase().execSQL("DROP TABLE IF EXISTS " + TABLE_INSTRUCTS);
    }


}
