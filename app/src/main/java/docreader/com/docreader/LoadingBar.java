package docreader.com.docreader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Administrator on 2017-12-14.
 */

public class LoadingBar {
    /**
     * @param activity 当前的activity，和等待标题
     * @author 陈杰柱
     */
    public Dialog getLoadingBar(Activity activity, String title, String content) {
        View view = LayoutInflater.from(activity).inflate(R.layout.loading, null);
        Dialog dialog = new AlertDialog.Builder(activity).setTitle(title).setView(view).create();
        if(content != null){
            ((TextView) view.findViewById(R.id.tv_loading_content)).setText(content);
        }
        return dialog;
    }
}