package docreader.com.docreader.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.sunflower.FlowerCollector;

import docreader.com.docreader.DocItem;
import docreader.com.docreader.LoadingBar;
import docreader.com.docreader.R;
import docreader.com.docreader.Util;
import docreader.com.docreader.adapters.ItemAdapter;
import docreader.com.docreader.database.DBHelper;


public class MainActivity extends AppCompatActivity {
    private static String APPNAME = MainActivity.class.getSimpleName();

    private List<DocItem> mDocItemList;
//    private String[] mDocStrArr = {"电饭锅且存在需", "开始跑步下雨撑伞走路", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视", "开门坐下喝茶吃点心看电视"};
    private SpeechSynthesizer mSpeechSyn;
    private Toast mToast;
    private ItemAdapter mItemAdapter;
    private TextView mStopBtnText;
    private Boolean mIsPlaying = false;
    private SpeechRecognizer mIat;
    List<List<String>> numList = new ArrayList<List<String>>();
    //        ArrayList<String> places = new ArrayList<String>(Arrays.asList("Buenos Aires", "Córdoba", "La Plata"));
    String[] numStrArr = new String[]{"一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二", "十三", "十四", "十五", "十六", "十七", "十八", "十九", "二十", "二十一", "二十二", "二十三", "二十四", "二十五", "二十六", "二十七", "二十八", "二十九", "三十"};
    DBHelper dbHelper;
//    private String dbPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + this.getPackageName() + "/";
    public static final String DATABASE_NAME = "docreader.db";
    private String dbPath;

    private InitListener mSpeechSynInitListener = new InitListener() {
        @Override
        public void onInit(int code) {
            if(code != ErrorCode.SUCCESS){
                showTip("speechSyn初始化错误码：" + code);
            }else{
                // 初始化成功，之后可以调用startSpeaking方法
                // 注：有的开发者在onCreate方法中创建完合成对象之后马上就调用startSpeaking进行合成，
                // 正确的做法是将onCreate中的startSpeaking调用移至这里
            }
        }
    };

    private InitListener mInitListener = new InitListener() {

        @Override
        public void onInit(int code) {
            if (code != ErrorCode.SUCCESS) {
                showTip("初始化失败，错误码：" + code);
            }
        }
    };


    public void showTip(final String str) {
        mToast.setText(str);
        mToast.show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermissions();

        mStopBtnText = (TextView)findViewById(R.id.stopBtnText);
        initTop();

        mSpeechSyn = SpeechSynthesizer.createSynthesizer(MainActivity.this, mSpeechSynInitListener);
        setParam();
        mToast = Toast.makeText(this,"",Toast.LENGTH_SHORT);

        mIat = SpeechRecognizer.createRecognizer(MainActivity.this, mInitListener);
        setParamForMIat();

        tempHardCodeInit();
        initNumList();
        initDatabase();

//        String pre_title = getResources().getString(R.string.test_doc_prepare_title);
//        String pre_content = getResources().getString(R.string.test_doc_prepare_content);
//        String pro_title = getResources().getString(R.string.test_doc_process_title);
//        String pro_content = getResources().getString(R.string.test_doc_process_content);
//        String doc_name = getResources().getString(R.string.test_doc_name);
//        如果有数据，就立刻初始化界面
        initRecyclerView();
        List<String> docNameList = dbHelper.getDocNameList();
        initDocListView(docNameList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
     }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Util.print("on option item selecteddddddddddddddddd");

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Log.d("Bind", "****************  setting 111111111111");
            final Dialog dialog = new LoadingBar().getLoadingBar(MainActivity.this,"数据加载中...",null);
            dialog.show();

            new Thread(new Runnable(){
                @Override
                public void run(){
                    Log.d("Bind", "1111111111111111");
                    dbHelper.dropAllTables();
                    dbHelper.onCreate(dbHelper.getWritableDatabase());
                    Log.d("Bind", "2222222222222222");
                    for(int i = 0; i < mDocItemList.size(); i++){
                        DocItem docItem = mDocItemList.get(i);
                        dbHelper.insertDocname(i, docItem.name);
//                        if(dbHelper.insertDocname(i, docItem.name)){
//                            Toast.makeText(this, "inserted one doc name = " + docItem.name, Toast.LENGTH_SHORT).show();
//                        }
                        for(int j = 0; j < docItem.instructs.size(); j++){
                            dbHelper.insertInstruct(i, docItem.instructs.get(j));
//                            if(dbHelper.insertInstruct(i, docItem.instructs.get(j))){
//                                Toast.makeText(this, "inserted one instructs for doc = " + docItem.name, Toast.LENGTH_SHORT).show();
//                            }
                        }
                    }
                    Log.d("Bind", "33333333333333");

                    //todo 获取 doc name list； 根据 文档index获取 instruct list

                    dbHelper.getDocNameList();
                    dbHelper.getInstructList(0);
                    dbHelper.getInstructList(1);
                    Log.d("Bind", "4444444444444444");
                    MainActivity.this.runOnUiThread(new Runnable(){
                        @Override
                        public void run(){
                            initDocListView(dbHelper.getDocNameList());
                        }
                    });

                    dialog.dismiss();
                }
            }).start();

            return true;
        }

        if(id == R.id.action_other){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
//    <string name="test_doc_name">江门供电局变电站日常巡视作业指导书</string>
//    <string name="test_doc_prepare_title">作业前准备</string>
//    <string name="test_doc_prepare_content">准备项, 出发前准备, 准备次项，技术资料，准备项内容，本站未消缺的缺陷清单</string>
//    <string name="test_doc_process_title">作业过程</string>
//    <string name="test_doc_process_content">序号，1，作业内容，了解运行方式及缺陷异常情况，作业标准，对有缺陷和异常的设备重点巡视。序号，2，作业内容，检查一次设备外观，作业标准，设备基础、支柱、构架安装牢固无下沉、倾斜变位、锈蚀，本体无变形、锈蚀、掉漆，接地良好。序号，3，作业内容，检查设备接头点、金具，作业标准，接头无发热、烧红现象，金具无变形和螺丝有无断损和脱落。序号，4，作业内容，检查设备绝缘子、瓷套，作业标准，无破损和灰尘污染，有无放电痕迹，无异常声响，序号，6，作业内容，检查充油设备油位、油压、油温，作业标准，无漏渗油，油位、油压、油温正常</string>

    private void initDatabase(){

                        //todo 1，链接数据库；
        // todo 2，读取数据库的doc name 来初始化界面，
                        // todo 3，如果没有数据库，创建数据库
        //todo   5,读取语音的时候用那些东西。
        //todo 点击setting按钮，删除数据库， 读取模拟doc list 创建数据库 （就用hard code的东西）
        //todo 真正初始化界面，和 初始化语音读取的list改用数据库接口。
        //todo 导入数据的时候，加个loading界面

        dbPath = getExternalFilesDir(null) + "/" + DATABASE_NAME;
        dbHelper = new DBHelper(this, dbPath);
        dbHelper.getData();
        Log.d("Bind", "db path =  **************8  - " + getDatabasePath("docreader.db"));

        Log.d("Bind", Environment.getDataDirectory().getAbsolutePath());
        Log.d("Bind", Environment.getExternalStorageDirectory().getAbsolutePath());
        Log.d("Bind", "didididid =  " + getExternalFilesDir(null));

    }

    private void initRecyclerView(){
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        llm.setStackFromEnd(true);
        llm.scrollToPosition(0);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);

        ///todo 下面的内容应该是 读取数据库之后创建的list*******************************************
        //oriign code
        mItemAdapter = new ItemAdapter(this, llm, new ArrayList<String>(), dbHelper);
        mRecyclerView.setAdapter(mItemAdapter);
    }

    private void initDocListView(List<String> docNameList){
//        如果已经存在list界面了，就要销毁了

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mItemAdapter.clearList();
        mItemAdapter.setList(docNameList);
        mItemAdapter.notifyDataSetChanged();
        mItemAdapter.getLayoutMgr().scrollToPosition(0);

//        LinearLayoutManager llm = new LinearLayoutManager(this);
//        llm.setOrientation(LinearLayoutManager.VERTICAL);
//        llm.setStackFromEnd(true);
//        llm.scrollToPosition(0);
//        mRecyclerView.setLayoutManager(llm);
//        mRecyclerView.setHasFixedSize(true);

        ///todo 下面的内容应该是 读取数据库之后创建的list*******************************************
       //oriign code
//        mItemAdapter = new ItemAdapter(this, llm, docNameList, dbHelper);


//        mRecyclerView.setAdapter(mItemAdapter);
    }

    @Override
    protected void onResume() {
        //移动数据统计分析
        FlowerCollector.onResume(MainActivity.this);
        FlowerCollector.onPageStart(APPNAME);
        super.onResume();
    }
    @Override
    protected void onPause() {
        //移动数据统计分析
        FlowerCollector.onPageEnd(APPNAME);
        FlowerCollector.onPause(MainActivity.this);
        super.onPause();
    }


    private void requestPermissions(){
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int permission = ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if(permission!= PackageManager.PERMISSION_GRANTED) {
//                        <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
//    <uses-permission android:name="android.permission.READ_CONTACTS" />
//    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
//    <uses-permission android:name="android.permission.WRITE_SETTINGS" />
//    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />

                    ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.LOCATION_HARDWARE,Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_SETTINGS,Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_CONTACTS,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION},0x0010);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 参数设置
     * @return
     */
    private void setParam(){
        // 清空参数
        mSpeechSyn.setParameter(SpeechConstant.PARAMS, null);
        // 根据合成引擎设置相应参数

        mSpeechSyn.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
            // 设置在线合成发音人
            //设置合成语速

        //设置播放器音频流类型
        // 设置播放合成音频打断音乐播放，默认为true
        mSpeechSyn.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "true");
        mSpeechSyn.setParameter(SpeechConstant.FORCE_LOGIN, "true");
        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        // 注：AUDIO_FORMAT参数语记需要更新版本才能生效
        mSpeechSyn.setParameter(SpeechConstant.AUDIO_FORMAT, "wav");
        mSpeechSyn.setParameter(SpeechConstant.TTS_AUDIO_PATH, Environment.getExternalStorageDirectory()+"/msc/tts.wav");
    }

    public void setParamForMIat() {
        // 清空参数
        mIat.setParameter(SpeechConstant.PARAMS, null);

        // 设置听写引擎
        mIat.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
        // 设置返回结果格式
        mIat.setParameter(SpeechConstant.RESULT_TYPE, "json");


        // 设置语言
        mIat.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
        // 设置语言区域



        // 设置语音前端点:静音超时时间，即用户多长时间不说话则当做超时处理
        mIat.setParameter(SpeechConstant.VAD_BOS, "10000");

        // 设置语音后端点:后端点静音检测时间，即用户停止说话多长时间内即认为不再输入， 自动停止录音
        mIat.setParameter(SpeechConstant.VAD_EOS, "10000");

        // 设置标点符号,设置为"0"返回结果无标点,设置为"1"返回结果有标点
        mIat.setParameter(SpeechConstant.ASR_PTT, "0");

        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        // 注：AUDIO_FORMAT参数语记需要更新版本才能生效
//        mIat.setParameter(SpeechConstant.AUDIO_FORMAT,"wav");
//        mIat.setParameter(SpeechConstant.ASR_AUDIO_PATH, Environment.getExternalStorageDirectory()+"/msc/iat.wav");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void initTop(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                if(getSpeechSyn().isSpeaking()){
                    if(mIsPlaying){
                        speechSynPause();
                    }else{
                        speechSynResume();
                    }
                }

            }
        });
    }

    private void tempHardCodeInit(){
        mDocItemList = new ArrayList<DocItem>();

        DocItem di1 = new DocItem();
        di1.name = "江门供电局变电站日常巡视作业指导书";
        List<String> ins1 = new ArrayList<>();
        ins1.add("作业内容，1，了解运行方式及缺陷异常情况，作业标准，对有缺陷和异常的设备重点巡视。");
        ins1.add("作业内容，2，检查一次设备外观，作业标准，设备基础、支柱、构架安装牢固无下沉、倾斜变位、锈蚀，本体无变形、锈蚀、掉漆，接地良好。");
        ins1.add("作业内容，3，检查一次设备外观，作业标准，接头无发热、烧红现象，金具无变形和螺丝有无断损和脱落。");
        ins1.add("作业内容，4，检查设备绝缘子、瓷套，作业标准，无破损和灰尘污染，有无放电痕迹，无异常声响");
        ins1.add("作业内容，5，检查避雷器，作业标准，动作次数和泄漏电流显示正确");
        ins1.add("作业内容，6，检查充油设备油位、油压、油温，作业标准，无漏渗油，油位、油压、油温正常");
        ins1.add("作业内容，7,检查充气设备气压,压力正常，SF6报警设备符合标准要求。");
        ins1.add("作业内容，8，检查主变压器，运行声音正常，呼吸器硅胶变色不超2/3，油封正常；瓦斯继电器无气体；冷却系统工作正常；");
        ins1.add("作业内容，9，检查开关、刀闸、地刀，位置指示正确与后台一致；带电显示器显示正确；开关储能正常；");
        ins1.add("作业内容，10，检查屏柜及机构箱，端子箱、机构箱、汇控箱密封良好无受潮，防潮装置运行正常。");
        ins1.add("作业内容，11，检查各功能室，无渗漏水；门窗关闭良好；空调运行正常；");
        ins1.add("作业内容，12，检查通信设备、继电保护及自动装置，压板投退正确，运行正常，无异常告警信号，保护装置充电指示正常。");
        ins1.add("作业内容，13，检查站内直流系统，充电机正常工作，各表计、指示灯显示正确，绝缘监测无异常；蓄电池组无异常；");
        ins1.add("作业内容，14，检查站内交流系统，切换装置各指示灯与实际位置相应，控制模块无异常死机现象");
        ins1.add("作业内容，15，检查五防及监控系统，1、五防主机 ：a) 检查主机电脑的防误闭锁装置是否在投入位置。 b) 对于微机防误装置，检查主机电脑与综自后台通讯应正常。 c) 检查主机接于不间断电源，且电源运行正常。 d) 主机配套打印机运行正常，硒鼓或墨盒充足，可正常打印防误系统相关信息。\n" +
                "2、五防模拟图： 检查模拟图内设备（包括开关、刀闸、地刀、临时接地桩等）位置与站内设备实时位置一致，设备位置状态指示灯显示正常。\n" +
                "3、 电脑钥匙 ：a) 检查钥匙外观正常，无破损；开启电脑钥匙电源，检查其屏幕显示正常、语音提示正常。b) 检查充电器正常，钥匙能正常充电。\n" +
                "4、编码锁 ：a) 检查编码锁无破损、外表无生锈、无积水，防雨罩完好并掩盖牢固。b) 检查编码锁是否齐全。\n" +
                "5、配套金具 ： 检查金具部件无锈蚀、变形。\n" +
                "6、 临时接地桩： 检查接地桩外观无锈蚀、变形。\n" +
                "7、 解锁用具：a) 检查解锁用具存放地点，确保解锁钥匙或用具存放于专用钥匙箱，且钥匙箱外封条完好，封条上查封日期清晰明了并有查封人员签名。\n" +
                "8、检查后台监控无异常信号。[2016.7.12修编此项内容。]");
        di1.instructs = ins1;
        mDocItemList.add(di1);

        DocItem di2 = new DocItem();
        di2.name = "45-江门供电局变电站防风防汛特巡作业指导书";
        List<String> ins2 = new ArrayList<>();
        ins2.add("作业内容，1，施工现场，作业标准，根据防风防汛相应等级及上级要求核实施工现场已结束（或间断）工作票，施工现场材料机具已按要求放置好；临时措施已拆除；");
        ins2.add("作业内容，2，检查站内户外场地、建筑物，作业标准，1、无大风可吹起飘挂物；对可能被风刮起的物体（包括施工单位的设施），必须采取措施加以固定；对于施工单位的物资、设施，要求施工单位应根据需要加固，必要时拆除。\n" +
                "2、高大树木无倒塌风险。\n" +
                "3、门窗关好并上锁。\n" +
                "4、场地端子箱、机构箱、汇控柜密封性良好；箱体没有破损，无渗漏水，密封条无损坏变形，驱潮设施运行正常，电缆封堵正常。\n");
        ins2.add("作业内容，3，检查应急工具，作业标准，铁铲、铁锹等应急工具完好");
        ins2.add("作业内容，4，检查户外电缆沟、排水沟、下水管、排水泵，作业标准，1、排水顺畅、无积水现象；有积水及时进行排除。\n" +
                "2、沙井无堆积物；有积水及时进行排除。\n" +
                "3、建筑物应无渗漏；排水顺畅；有积水及时进行排除。\n" +
                "4、抽水泵电源正常，试验排水泵正常工作。\n");
        ins2.add("作业内容，5，检查变电站周边，作业标准，1、山体、护坡、挡土墙应无异常情况。\n" +
                "2、观察附近高层居民楼，应无可飘落物。\n" +
                "3、围墙周围应无飘浮物，防止吹到站内设备上造成短路故障，站边黑点（简易棚架等）应已采取相应防护措施。\n");
        ins2.add("作业内容，6，检查避雷器动作情况，1、大风、大雨后，检查各侧避雷器动作情况，泄漏电流应符合要求。\n");
        ins2.add("作业内容，7，填写相关记录，发现异常、缺陷情况及时记录并向上级汇报处理，记录至生产管理信息系统，工作内容、结果记录清晰；报告及时，处理有效，发现异常情况应向上级汇报。");
        di2.instructs = ins2;

        mDocItemList.add(di2);

        DocItem di3 = new DocItem();
        di3.name = "江门供电局变电站主变冷却器电源切换试验作业指导书";
        List<String> ins3 = new ArrayList<>();
        ins3.add("作业内容，1，联系调度，作业标准，联系当值调度,说明要进行主变冷却器电源切换，监控后台会有信号上传。");
        ins3.add("作业内容，2，核准切换条件，作业标准，强迫油循环冷却方式主变油泵启动时，禁止切换试验。主变负荷超过额定值80%时，禁止切换试验。");
        ins3.add("作业内容，3，确认两路冷却器电源正常，作业标准，站内交流系统应正常，用万用表测量冷却器两路总电源正常。");
        ins3.add("作业内容，4，确认冷却风扇正常，作业标准，手动启动主变冷却风扇，检查风扇启动正常。");
        ins3.add("作业内容，5，检查低压配电室2P #2站用变馈线II屏：“2QF13  #2主变通风电源空气开关”、 “2QF17  #1主变通风、有载调压电源空气开关”确在合上位置，作业标准，指示在合上位置");
        ins3.add("作业内容，6，检查低压配电室5P #1站用变馈线I屏：“5QF15  #2主变通风电源空气开关”、 “5QF11  #1主变通风电源空气开关”确在合上位置，指示在合上位置");
        ins3.add("作业内容，7，检查#1主变冷却装置控制箱控制电源开关Q3确在合上位置，指示在合上位置");
        ins3.add("作业内容，8，将#1主变冷却装置控制箱电源投入控制方式由“I电源工作II电源备用”位置切换到“II电源工作I电源备用” 位置。，指示在正确位置。");
        ins3.add("作业内容，9，检查#1主变冷却风扇运行正常。，正确。");
        ins3.add("作业内容，10，将#1主变冷却装置控制箱电源投入控制方式由“II电源工作I电源备用”位置切换到“I电源工作II电源备用” 位置，指示在正确位置。");
        di3.instructs = ins3;

        mDocItemList.add(di3);


        DocItem di4 = new DocItem();
        di4.name = "江门供电局变电站站用交流电源备自投试验检查作业指导书";
        List<String> ins4 = new ArrayList<>();
        ins4.add("作业内容，1，核查站内交流系统无异常缺陷，作业标准，核查站内交流系统无异常缺陷");
        ins4.add("作业内容，2，在4P #1站用变进线屏检查11QF #1站用变进线开关、12QF #2站用变进线开关在合上位置，作业标准，开关位置正确");
        ins4.add("作业内容，3，在4P #1站用变进线屏检查#1站用变电源指示灯、#2站用变电源指示灯亮，作业标准，检查指示灯已亮");
        ins4.add("作业内容，4，在4P #1站用变进线屏检查1ATS合站用电#1电源灯亮、1ATS合站用电#2电源灯不亮，作业标准，检查指示灯指示正确");
        ins4.add("作业内容，5，在4P #1站用变进线屏检查#1、#2进线电压正常，作业标准，数据显示正常");
        ins4.add("作业内容，6，在4P #1站用变进线屏将电源智能控制器模式切换至“固定电源2”，切换正确");
        ins4.add("作业内容，7，在4P #1站用变进线屏检查1ATS 切换正确，1ATS合站用电#1电源灯不亮、1ATS合站用电#2电源灯亮，切换正确，指示灯指示正确");
        ins4.add("作业内容，8，检查4P #1站用变进线屏电流电压正常，数据显示正常");
        ins4.add("作业内容，9，在4P #1站用变进线屏将电源智能控制器模式切换至“自动电源1”，切换正确");
        ins4.add("作业内容，10，在4P #1站用变进线屏检查1ATS 切换正确，1ATS合站用电#1电源灯亮、1ATS合站用电#2电源灯不亮，切换正确，指示灯指示正确");
        di4.instructs = ins4;

        mDocItemList.add(di4);


        DocItem di5 = new DocItem();
        di5.name = "江门供电局变电站事故照明切换检查作业指导书";
        List<String> ins5 = new ArrayList<>();
        ins5.add("作业内容，1，在低压配电室1P #2站用变馈线I屏检查高压室事故照明开关QF6在合上位置，作业标准，位置正确");
        ins5.add("作业内容，2，在低压配电室1P #2站用变馈线I屏检查主控室事故照明开关QF7在合上位置，作业标准，位置正确");
        ins5.add("作业内容，3，在低压配电室1P #2站用变馈线I屏检查QF1逆变器直流电源输入开关、QF2逆变器交流电源输入开关在合上位置；逆变器“市电”、“旁路”指示灯亮。，作业标准，位置正确、指示灯指示正确");
        ins5.add("作业内容，4，在低压配电室1P #2站用变馈线I屏断开逆变器交流电源输入开关QF2。，作业标准，位置正确");
        ins5.add("作业内容，5，在1P #2站用变馈线I屏检查逆变器逆变指示灯亮。，作业标准，在1P #2站用变馈线I屏检查逆变器逆变指示灯亮。");
        ins5.add("作业内容，6，在低压配电室合上事故照明电源开关,检查各事故照明灯是否正常，并记录损坏的照明灯。，位置正确");
        ins5.add("作业内容，7，检查完毕后，在低压配电室断开事故照明电源开关，位置正确");
        ins5.add("作业内容，8，在主控室合上事故照明电源开关,检查各事故照明灯是否正常，并记录损坏的照明灯。，逐一检查");
        ins5.add("作业内容，9，检查完毕后，在主控室断开事故照明电源开关，位置正确");
        ins5.add("作业内容，10，在高压室合上事故照明电源开关,检查各事故照明灯是否正常，并记录损坏的照明灯。，逐一检查");
        di5.instructs = ins5;

        mDocItemList.add(di5);

        DocItem di6 = new DocItem();
        di6.name = "江门供电局变电站UPS电源切换作业指导书";
        List<String> ins6 = new ArrayList<>();
        ins6.add("作业内容，1，在7P 逆变器屏检查UPS不间断电源主机、从机运行正常。，作业标准，设备在正常运行状态。");
        ins6.add("作业内容，2，在7P 逆变器屏后断开电源屏交流输入开关K1，作业标准，位置正确");
        ins6.add("作业内容，3，在7P 逆变器屏检查UPS不间断电源主机“交－直”灯灭、“直－交”灯亮，作业标准，指示灯指示正确。");
        ins6.add("作业内容，4，在7P 逆变器屏检查UPS不间断电源从机“交－直”灯灭、“直－交”灯亮，作业标准，指示灯指示正确");
        ins6.add("作业内容，5，在7P 逆变器屏检查各负载工作正常，作业标准，各负载设备正常工作");
        ins6.add("作业内容，5,在7P 逆变器屏检查各负载工作正常,各负载设备正常工作。");
        ins6.add("作业内容，6,在7P 逆变器屏后合上电源屏交流输入开关K1,位置正确");
        ins6.add("作业内容，7,在7P 逆变器屏检查UPS不间断电源主机“交－直”灯亮、“直－交”灯亮,指示灯指示正确。");
        ins6.add("作业内容，8,在7P 逆变器屏检查UPS不间断电源从机“交－直”灯亮、“直－交”灯亮,指示灯指示正确");
        ins6.add("作业内容，9,检查UPS系统运行正常,系统运行运转正常，数据更新正常。");
        ins6.add("作业内容，10,试验完毕后，在生产管理信息系统填写相关记录簿,试验内容、试验结果记录清晰");
        di6.instructs = ins6;

        mDocItemList.add(di6);

        for(int i = 1; i <20; i++){
            DocItem di = new DocItem();
            di.name = getResources().getString(R.string.test_doc_name) + i;
            List<String> ins = new ArrayList<>();
            ins.add("作业内容，1，施工现场，作业标准，根据防风防汛相应等级及上级要求核实施工现场已结束（或间断）工作票，施工现场材料机具已按要求放置好；临时措施已拆除；");
            ins.add("作业内容，2，检查站内户外场地、建筑物，作业标准，无大风可吹起飘挂物；对可能被风刮起的物体（包括施工单位的设施），必须采取措施加以固定；对于施工单位的物资、设施，要求施工单位应根据需要加固，必要时拆除。");
            ins.add("作业内容，3，检查应急工具，作业标准，检查应急工具，铁铲、铁锹等应急工具完好。");
            ins.add("作业内容，4，检查户外电缆沟、排水沟、下水管、排水泵，作业标准，排水顺畅、无积水现象；有积水及时进行排除。");
            ins.add("作业内容，5，检查避雷器，动作次数和泄漏电流显示正确");
            ins.add("作业内容，6，检查充油设备油位、油压、油温，无漏渗油，油位、油压、油温正常");
            ins.add("作业内容，7，检查充气设备气压，压力正常，SF6报警设备符合标准要求。");
            ins.add("作业内容，8，检查主变压器，运行声音正常，呼吸器硅胶变色不超2/3，油封正常；瓦斯继电器无气体；冷却系统工作正常；");
            ins.add("作业内容，9，检查开关、刀闸、地刀，位置指示正确与后台一致；带电显示器显示正确；开关储能正常；");
            ins.add("作业内容，10，检查屏柜及机构箱，端子箱、机构箱、汇控箱密封良好无受潮，防潮装置运行正常。");
            di.instructs = ins;
            di.path = "c:/dkfjkdjfk";
            mDocItemList.add(di);
        }
    }

    public SpeechSynthesizer getSpeechSyn(){
        return mSpeechSyn;
    }

    public SpeechRecognizer getSpeechRecogn(){return mIat;}

    public void SetStopBtnText(String str){
        mStopBtnText.setText(str);
    }

    public void SetIsPlaying(Boolean isPlaying){
        mIsPlaying = isPlaying;
    }

    public void speechSynPause(){
        getSpeechSyn().pauseSpeaking();
        mIsPlaying = false;
    }
    public void speechSynResume(){
        getSpeechSyn().resumeSpeaking();
        mIsPlaying = true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        Log.d("Bind", "****************  result code = " + resultCode);
        Log.d("Bind", "****************  requestCode code = " + requestCode);
    }

    private void initNumList(){
        for(int i = 0; i < 30; i++){
            List<String> l = new ArrayList<>();
            l.add(numStrArr[i]);
            l.add(String.valueOf(i+1));
            numList.add(l);
        }
    }

    public int tryGetNum(String text){
        int ret = -1;

        for(int i = 0; i < numList.size(); i++ ){// List<String> strList : numList){
            List<String> strList = numList.get(i);
            for(String str : strList){
                if(text.contains(str)){
                    ret = i;
                }
            }
            if(ret > 0) break;
        }

        return ret;
    }

}
